package com.crpg.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Rainy on 10/16/17.
 */

public class CTextView extends TextView {
    public CTextView(Context context) {
        super(context);
    }

    public CTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
