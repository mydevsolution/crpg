package com.crpg.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crpg.R;
import com.crpg.data.Character;
import com.crpg.data.DataCenter;
import com.crpg.data.User;
import com.crpg.network.NetworkClient;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Rainy on 10/15/17.
 */

public class CharacterItemView extends RelativeLayout {
    TextView tvTapCreate, tvCharacterName;
    ImageView ivAvatar, ivCharDelete, ivEdit;
    FrameLayout flActive;
    int mIndex;
    Character mCharacter;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Context mContext;
    private ProgressDialog dialog;
    private CharacterItemViewDelegate delegate;
    private PublishSubject<Object> initSubject = PublishSubject.create();

    public CharacterItemView(Context context) {
        super(context);
        init();
    }

    public CharacterItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CharacterItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View rootView = inflate(getContext(), R.layout.item_character_layout, this);
        tvTapCreate = rootView.findViewById(R.id.tvTapCreate);
        tvCharacterName = rootView.findViewById(R.id.tvCharacterName);
        ivAvatar = rootView.findViewById(R.id.ivAvatar);
        ivCharDelete = rootView.findViewById(R.id.ivCharDelete);
        ivEdit = rootView.findViewById(R.id.ivEdit);
        flActive = rootView.findViewById(R.id.flActive);
        setupUI();
        bindingData();
    }

    private void setupUI() {
        tvCharacterName.setText("");
        ivCharDelete.setVisibility(INVISIBLE);
        ivEdit.setVisibility(INVISIBLE);
        tvCharacterName.setVisibility(INVISIBLE);
        ivAvatar.setImageResource(R.drawable.bg_border_char_empty);
        tvTapCreate.setVisibility(VISIBLE);
    }

    public void doFirstSetupEvent() {
        initSubject.onNext(new Object());
    }

    private void bindingData() {

        compositeDisposable.add(Observable.merge(RxView.clicks(ivAvatar).share(), RxView.clicks(tvTapCreate), initSubject)
                .debounce(300, TimeUnit.MILLISECONDS)
                .doOnNext(o -> {
                    if (mCharacter == null) showDialog();
                })
                .flatMap(o -> mCharacter == null ? DataCenter.Companion.getInstance().createCharacter()
                        : DataCenter.Companion.getInstance().setActiveCharacter(mCharacter))
                .doOnNext(character -> mCharacter = character)
                .flatMap(character ->
                        NetworkClient.Companion.getInstance().getCurrentUser()
                        .map(User::getActive_char_ID)).distinctUntilChanged()
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
//                    Log.d(CharacterItemView.class.getSimpleName(), s + "    " + mCharacter != null ? mCharacter.getChar_ID() : "");
                    setCharacter(mCharacter, mCharacter != null && s.equalsIgnoreCase(mCharacter.getChar_ID()));
                    hideDialog();
                }));

        compositeDisposable.add(
                RxView.clicks(ivCharDelete).flatMap(o -> {
                    showDialog();
                    return DataCenter.Companion.getInstance().deleteCharacter(mCharacter.getChar_ID());
                })
                        .doOnNext(s -> {
                            if (s.isEmpty()) {
                                mCharacter = null;
                                setupUI();
                            }
                            hideDialog();
                        })
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe());

        compositeDisposable.add(RxView.clicks(ivEdit).subscribe(o -> {
            if (delegate!= null) {
                delegate.goToCharacter(mCharacter);
            }
        }));
    }

    @Override
    protected void detachViewFromParent(int index) {
        super.detachViewFromParent(index);
        compositeDisposable.clear();
    }

    public void setContext(Context context) {
        mContext = context;
    }

    private void showDialog() {
        if (delegate != null) {
            delegate.showLoading();
        }
    }

    private void hideDialog() {
        if (delegate != null) {
            delegate.hideLoading();
        }
    }


    public void setCharacter(Character character, Boolean isActive) {
        if (mContext == null || character == null) return;
        mCharacter = character;
        tvTapCreate.setVisibility(INVISIBLE);
        tvCharacterName.setVisibility(VISIBLE);
        ivEdit.setVisibility(VISIBLE);
        ivCharDelete.setVisibility(isActive ? INVISIBLE : VISIBLE);
        tvCharacterName.setText(character.getChar_name());
        Glide.with(mContext)
                .load(character.getAvatar())
                .placeholder(R.drawable.img_chat_user_sample)
                .into(ivAvatar);
        flActive.setVisibility(isActive ? VISIBLE : GONE);
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setDelegate(CharacterItemViewDelegate listener) {
        this.delegate = listener;
    }

    public interface CharacterItemViewDelegate {
        void showLoading();

        void hideLoading();

        void goToCharacter(Character character);
    }
}
