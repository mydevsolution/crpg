package com.crpg.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crpg.R;
import com.crpg.adapters.AvatarAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rainy on 11/4/17.
 */

public class ChooseAvatarView extends RelativeLayout {

    public ChooseAvatarView(Context context) {
        super(context);
        init();
    }

    public ChooseAvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChooseAvatarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View rootView = inflate(getContext(), R.layout.layout_choose_avatar, this);
        GridView gridAvatars = rootView.findViewById(R.id.gridAvatars);
        TextView tvSelect = rootView.findViewById(R.id.tvSelect);
        List<Integer> listResource = new ArrayList<>();
        for (int i = 0; i < 8 ; i++) {
            listResource.add(R.drawable.img_sample_user);
        }
        AvatarAdapter avatarAdapter = new AvatarAdapter(listResource, getContext());
        gridAvatars.setAdapter(avatarAdapter);

        tvSelect.setOnClickListener(v -> {
            this.setVisibility(GONE);
        });
    }
}
