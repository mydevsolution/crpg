package com.crpg.data

/**
 * Created by Rainy on 10/15/17.
 */

/*

- users:
    - user_ID:
        - username
        - online_status:
        - active_char_ID:
        - characters:
            - char_ID: true
            - char_ID_:true
        - friends
            - char_ID: true
        - story_rooms:
            - room_ID: true
        - favorite_rooms"
            - room_ID: true
- story_rooms:
    - room_ID
        - characters:
            - char_ID:
            - char_ID:
        - room_pass:
        - room_infor:
        - room_title:
        - room_admin: char_ID
        - room_deleted: false
- characters:
    - char_ID:
        - avatar:
        - user_ID:
        - char_name:
- messages:
    - room_ID
        - message_ID:
            - content:
            - sender_ID(char_ID):
            - sender_name:
            - receiver_ID(room_ID):
            - time_stamp:
        - message_ID:

*/

data class BaseIDModel(var key: String, var value: Boolean) {
    fun toJSON(): MutableMap<String, Any> {
        val mapper: MutableMap<String, Any> = mutableMapOf()
        mapper[""]
        return mapper
    }
}

data class User(var user_ID: String, var active_char_ID: String, var username: String, var online_status: Boolean,
                var characters: List<BaseIDModel>, var friends: List<BaseIDModel>, var story_rooms: List<BaseIDModel>) {

    constructor(username: String, online_status: Boolean,
                characters: List<BaseIDModel>, friends: List<BaseIDModel>, story_rooms: List<BaseIDModel>) :
            this("", "", username, online_status, characters, friends, story_rooms)

    constructor() : this("", "", "",  false, ArrayList(), ArrayList(), ArrayList())

    var error: String = ""
    var favorite_rooms = ArrayList<BaseIDModel>()
}

data class StoryRoom(var room_ID: String, var characters: List<BaseIDModel>, var room_pass: String, var room_infor: String, var room_title: String) {
    constructor(characters: List<BaseIDModel>, room_pass: String, room_infor: String, room_title: String) :
            this("", characters, room_pass, room_infor, room_title)

    constructor() : this("", ArrayList(), "", "", "")

    var error: String = ""
    var room_admin: String = ""
    var room_deleted = false
}

data class Character(var char_ID: String, var avatar: String, var user_ID: String, var char_name: String) {
    constructor(avatar: String, user_ID: String) : this("", avatar, user_ID, "")
    constructor() : this("", "", "", "")

    var error: String = ""
    var back_story: String = ""
    var ability: String = ""
    var item: String = ""
}

data class Message(var room_ID: String, var message_ID: String, var content: String, var sender_ID: String, var sender_name: String, var receiver_ID: String, var time_stamp: Long) {
    constructor(content: String, sender_ID: String, sender_name: String, receiver_ID: String, time_stamp: Long) :
            this("", "", content, sender_ID, sender_name, receiver_ID, time_stamp)

    constructor() : this("", "", "", "", -1)

    var error: String = ""
}

/*===========*/

fun User.toJSON(): MutableMap<String, Any> {
    val mapper: MutableMap<String, Any> = mutableMapOf()
    mapper.put("username", username)
    mapper.put("online_status", online_status)
    mapper.put("active_char_ID", active_char_ID)
    if (characters.size > 0) {
        val charMap: MutableMap<String, Any> = mutableMapOf()
        characters.forEachIndexed { _, baseIDModel ->
            charMap[baseIDModel.key] = baseIDModel.value
        }
        mapper.put("characters", charMap)
    }

    if (friends.size > 0) {
        val friendMap: MutableMap<String, Any> = mutableMapOf()
        friends.forEachIndexed { _, baseIDModel ->
            friendMap[baseIDModel.key] = baseIDModel.value
            friendMap.put(baseIDModel.key, true)
        }
        mapper.put("friends", friendMap)
    }

    if (story_rooms.size > 0) {
        val roomMap: MutableMap<String, Any> = mutableMapOf()
        story_rooms.forEachIndexed { _, baseIDModel ->
            roomMap.put(baseIDModel.key, true)
        }
        mapper.put("story_rooms", roomMap)
    }

    if (favorite_rooms.size > 0) {
        val roomMap: MutableMap<String, Any> = mutableMapOf()
        favorite_rooms.forEachIndexed { _, baseIDModel ->
            roomMap.put(baseIDModel.key, true)
        }
        mapper.put("favorite_rooms", roomMap)
    }

    return mapper
}

fun Character.toJSON(): MutableMap<String, Any> {
    val mapper: MutableMap<String, Any> = mutableMapOf()
    mapper["avatar"] = avatar
    mapper["user_ID"] = user_ID
    mapper["char_name"] = char_name
    mapper["item"] = item
    mapper["ability"] = ability
    mapper["back_story"] = back_story
    return mapper
}

fun StoryRoom.toJSON(): MutableMap<String, Any> {
    val mapper: MutableMap<String, Any> = mutableMapOf()
    if (characters.size > 0) {
        val charMap: MutableMap<String, Any> = mutableMapOf()
        characters.forEachIndexed { _, baseIDModel ->
            charMap[baseIDModel.key] = baseIDModel.value
        }
        mapper["characters"] = charMap
    }
    mapper["room_infor"] = room_infor
    mapper["room_pass"] = room_pass
    mapper["room_title"] = room_title
    mapper["room_admin"] = room_admin
    mapper["room_deleted"] = room_deleted
    return mapper
}

fun Message.toJSON(): MutableMap<String, Any> {
    val mapper: MutableMap<String, Any> = mutableMapOf()
    mapper["sender_ID"] = sender_ID
    mapper["sender_name"] = sender_name
    mapper["receiver_ID"] = receiver_ID
    mapper["content"] = content
    mapper["time_stamp"] = time_stamp
    return mapper
}
