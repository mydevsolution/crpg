package com.crpg.data

import com.google.firebase.database.DataSnapshot

/**
 * Created by Rainy on 10/16/17.
 */
class ConverterUtils private constructor() {
    companion object {
        val instance = ConverterUtils()
        private val TAG = ConverterUtils::class.java.simpleName
    }

    fun userFromDataSnapshot(snapshot: DataSnapshot): User {
        val user = User()
        snapshot.children.forEach { childSnap ->
            if (childSnap.key.contains("username")) {
                user.username = childSnap.value as String
            }

            if (childSnap.key.contains("active_char_ID")) {
                user.active_char_ID = childSnap.value as String
            }

            if (childSnap.key.contains("online_status")) {
                user.online_status = childSnap.value as Boolean
            }

            if (childSnap.key.contains("characters")) {
                user.characters = listBaseIDFrom(childSnap)
            }

            if (childSnap.key.contains("story_rooms")) {
                user.story_rooms = listBaseIDFrom(childSnap)
            }

            if (childSnap.key.contains("friends")) {
                user.friends = listBaseIDFrom(childSnap)
            }

            if (childSnap.key.contains("favorite_rooms")) {
                user.favorite_rooms = listBaseIDFrom(childSnap)
            }
        }
        return user
    }

    fun listUserFromSnapShot(snapshot: DataSnapshot): List<User> {
        val listUser = ArrayList<User>()
        snapshot.children.forEach { childSnapShot ->
            val user = userFromDataSnapshot(childSnapShot)
            user.user_ID = childSnapShot.key
            listUser.add(user)
        }
        return listUser
    }

    fun roomFromSnapShot(snapshot: DataSnapshot): StoryRoom {
        val room = StoryRoom()
        snapshot.children.forEach { childSnap ->
            if (childSnap.key.contains("room_pass")) {
                room.room_pass = childSnap.value as String
            }

            if (childSnap.key.contains("room_infor")) {
                room.room_infor = childSnap.value as String
            }

            if (childSnap.key.contains("room_title")) {
                room.room_title = childSnap.value as String
            }

            if (childSnap.key.contains("room_admin")) {
                room.room_admin = childSnap.value as String
            }

            if (childSnap.key.contains("room_deleted")) {
                room.room_deleted = childSnap.value as Boolean
            }

            if (childSnap.key.contains("characters")) {
                room.characters = listBaseIDFrom(childSnap)
            }
        }
        return room
    }

    fun listRoomFromSnapShot(snapshot: DataSnapshot): List<StoryRoom> {
        val listRoom = ArrayList<StoryRoom>()
        snapshot.children.forEach { childSnapShot ->
            val room = roomFromSnapShot(childSnapShot)
            room.room_ID = childSnapShot.key
            listRoom.add(room)
        }
        return listRoom
    }

    fun characterFromSnapShot(snapshot: DataSnapshot): Character {
        val character = Character()
        character.char_ID = snapshot.key
        snapshot.children.forEach { childSnap ->
            if (childSnap.key.contains("avatar")) {
                character.avatar = childSnap.value as String
            }

            if (childSnap.key.contains("user_ID")) {
                character.user_ID = childSnap.value as String
            }

            if (childSnap.key.contains("char_name")) {
                character.char_name = childSnap.value as String
            }

            if (childSnap.key.contains("item")) {
                character.item = childSnap.value as String
            }

            if (childSnap.key.contains("ability")) {
                character.ability = childSnap.value as String
            }

            if (childSnap.key.contains("back_story")) {
                character.back_story = childSnap.value as String
            }
        }
        return character
    }

    fun messageFromSnapShot(snapshot: DataSnapshot): Message {
        val message = Message()
        snapshot.children.forEach { childSnap ->
            if (childSnap.key.contains("content")) {
                message.content = childSnap.value as String
            }

            if (childSnap.key.contains("sender_ID")) {
                message.sender_ID = childSnap.value as String
            }

            if (childSnap.key.contains("sender_name")) {
                message.sender_name = childSnap.value as String
            }

            if (childSnap.key.contains("receiver_ID")) {
                message.receiver_ID = childSnap.value as String
            }

            if (childSnap.key.contains("time_stamp")) {
                message.time_stamp = childSnap.value as Long
            }
        }
        return message
    }


    fun listBaseIDFrom(dataSnapshot: DataSnapshot): ArrayList<BaseIDModel> {
        val listBaseArray = ArrayList<BaseIDModel>()
        dataSnapshot.children.forEach { snap ->
            val base = BaseIDModel(snap.key, snap.value as Boolean)
            if (base.value)
                listBaseArray.add(base)
        }
        return listBaseArray
    }
}