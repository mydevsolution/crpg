package com.crpg.data

import com.crpg.network.NetworkClient
import io.reactivex.Observable

/**
 * Created by Rainy on 10/15/17.
 */
class DataCenter private constructor() {

    val network: NetworkClient = NetworkClient.instance

    companion object {
        val instance = DataCenter()
        private val TAG = DataCenter::class.java.simpleName
    }

    fun signIn(username: String, password: String): Observable<User> {
        return network.signIn(username, password)
                .flatMap { error ->
                    if (error.isEmpty()) {
                        network.getCurrentUser()
                    } else {
                        val user = User()
                        user.error = error
                        Observable.just(user)
                    }
                }.map { user ->
            user.online_status = true
            user
        }.flatMap { user ->
            network.updateUser(user, false)
        }
    }

    fun signUp(username: String, password: String): Observable<User> {
        return network.createNewUser(username, password)
                .map { errorStr ->
                    if (errorStr.toLowerCase().contains("is already in use")) {
                        return@map "Username is taken"
                    } else {
                        errorStr
                    }
                }
//                .flatMap { errorStr ->
//                    if (errorStr.isEmpty()) {
//                        network.createCharacter()
//                    } else {
//                        val user = User()
//                        user.error = errorStr
//                        Observable.just(user)
//                    }
//                }
//                .map { activeChar ->
//                    val userId = network.getCurrentUserID()
//                    val emptyList = ArrayList<BaseIDModel>()
//                    val charList = ArrayList<BaseIDModel>()
//                    charList.add(BaseIDModel(activeChar.char_ID, true))
//                    val user = User(userId, activeChar.char_ID, com.crpg.utils.Utils.mapEmailToUsername(username), true, charList, emptyList, emptyList)
//                    user
//                }
                .map { errorStr ->
                    val user: User
                    if (errorStr.isEmpty()) {
                        val userId = network.getCurrentUserID()
                        val emptyList = ArrayList<BaseIDModel>()
                        val charList = ArrayList<BaseIDModel>()
                        user = User(userId, "", com.crpg.utils.Utils.mapEmailToUsername(username), true, charList, emptyList, emptyList)
                    } else {
                        user = User()
                        user.error = errorStr
                    }
                    user
                }
                .flatMap { user ->
                    if (user.error.isEmpty()) {
                        network.updateUser(user, false)
                    } else {
                        Observable.just(user)
                    }
                }
    }

    fun createNewRoom(roomName: String, roomInfor: String, roomPass: String): Observable<StoryRoom> {
        val room = StoryRoom(ArrayList(), roomPass, roomInfor, roomName)
        room.room_admin = network.getTempUser().active_char_ID
//        val chars = ArrayList<BaseIDModel>()
//        chars.add(BaseIDModel(NetworkClient.instance.getCurrentUserID(), true))
//        room.characters = chars
        return network.updateStoryRoom(room)
                .flatMap { updatedRoom ->
                    network.getCurrentUser()
                            .map { user ->
                                val rooms = user.story_rooms.toMutableList()
                                rooms.add(BaseIDModel(updatedRoom.room_ID, true))
                                user.story_rooms = rooms.toList()
                                user
                            }
                            .doOnNext { network.createEmptyMessageRoom(updatedRoom.room_ID) }
                            .flatMap { user -> network.updateUser(user, false) }
                            .map { updatedRoom }
                }
    }


    fun getAllRooms(): Observable<List<StoryRoom>> {
        return network.getAllRooms()
    }

    fun getCurrentUserRooms(): Observable<List<StoryRoom>> {
        return network.getCurrentUser()
                .flatMap { user ->
                    network.getStoryRooms(user)
                }
    }

    fun getAllUser(): Observable<List<User>> {
        return network.getAllUser()
    }

    fun getMessageFromRoom(room_ID: String): Observable<Message> {
        return network.getMessageFromRoom(room_ID)
    }

    fun sendMessage(content: String, room_ID: String, sender_name: String) {
        val message = Message(content, network.getCurrentUserID(), sender_name, room_ID, System.currentTimeMillis())
        network.sendMessage(message)
    }

    fun createCharacter(): Observable<Character> {
        return network.createCharacter()
    }

    fun getUserCharacters(): Observable<List<Character>> {
        return network.getCurrentUser()
                .flatMap { user ->
                    val listObservable = user.characters
                            .filter { char -> char.value }
                            .map { charIdModel -> network.getCharacter(charIdModel.key) }
                    Observable.zip(listObservable, { values ->
                        values.map { result -> result as Character }
                    })
                }
    }

    fun getMyFriends(): Observable<List<Character>> {
        return network.getCurrentUser()
                .flatMap { user ->
                    val listObservable = user.friends
                            .filter { char -> char.value }
                            .map { charIdModel -> network.getCharacter(charIdModel.key) }
                    Observable.zip(listObservable, { values ->
                        values.map { result -> result as Character }
                    })
                }
    }

    fun getRoomCharacter(roomID: String): Observable<List<Character>> {
        return network.getStoryRoom(roomID)
                .flatMap { room ->
                    val listObservable = room.characters
                            .filter { char -> char.value }
                            .map { charIdModel -> network.getCharacter(charIdModel.key) }
                    Observable.zip(listObservable, { values ->
                        values.map {
                            result -> result as Character }
                    })
                }
    }


    fun deleteCharacter(char_ID: String): Observable<String> {
        return network.deleteCharacter(char_ID)
    }

    fun setActiveCharacter(character: Character): Observable<Character> {
        return network.setActiveCharacter(character)
    }

    fun getCharacter(mCharID: String): Observable<Character> {
        return network.getCharacter(mCharID)
    }

    fun enterRoom(roomID: String): Observable<String> {
        return network.enterRoom(roomID)
    }

    fun exitRoom(roomID: String): Observable<String> {
        return network.exitRoom(roomID)
    }

}