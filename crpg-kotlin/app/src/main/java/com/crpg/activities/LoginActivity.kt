package com.crpg.activities

import android.content.Intent
import android.os.Bundle
import com.crpg.R
import com.crpg.data.DataCenter
import com.crpg.network.NetworkClient
import com.crpg.utils.Utils
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit

class LoginActivity : BaseActivity() {

    override fun setupUI() {

        if (!NetworkClient.instance.getCurrentUserID().isEmpty()) {
            gotoMain()
        }

        compositeDisposable.add(RxView.clicks(tvLogin)
                .doOnNext { tvLogin.isClickable = false }
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .filter {
                    val emptyCheck = Utils.checkEmptyUserAndPassword(etUsername.text.toString(), etPassword.text.toString())
                    if (!emptyCheck.isEmpty()) {
                        showToast(emptyCheck, false)
                        tvLogin.isClickable = true
                    }
                    emptyCheck.isEmpty()
                }
                .doOnNext {
                    showToast("Please wait logging you in...", true)
                }
                .filter { Utils.validateUserAndPassword(etUsername.text.toString(), etPassword.text.toString()) }
                .flatMap {
                    DataCenter.instance.signIn(Utils.mapUsernameToEmail(etUsername.text.toString()),
                            etPassword.text.toString())
                }
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { user ->
                    tvLogin.isClickable = true
                    if (user.error.isEmpty()) {
                        gotoMain()
                    } else {
                        showToast("Error: " + user.error, false)
                    }
                })

        compositeDisposable.add(RxView.clicks(ivEdit)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                    startActivity(intent)
                    finish()
                })

    }

    private fun gotoMain() {
        val intent = Intent(this@LoginActivity, MainMenuActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

}
