package com.crpg.activities

import android.os.Bundle
import android.view.ViewGroup
import com.crpg.R
import com.crpg.data.Character
import com.crpg.data.DataCenter
import com.crpg.data.User
import com.crpg.network.NetworkClient
import com.crpg.widget.CharacterItemView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_characters.*
import android.app.ProgressDialog
import android.content.Intent
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_login.*


/**
 * Created by Rainy on 10/15/17.
 */
open class CharactersActivity : BaseActivity(), CharacterItemView.CharacterItemViewDelegate {

    override fun goToCharacter(character: Character?) {
        character.let {
            val intent = Intent(this, CharacterEditActivity::class.java)
            intent.putExtra(CharacterEditActivity.CHAR_ID_EXTRA, character!!.char_ID)
            startActivity(intent)
            finish()
        }
    }

    private var dialog: ProgressDialog? = null

    override fun showLoading() {
        runOnUiThread {
            if (dialog == null)
                dialog = ProgressDialog(this);

            dialog!!.show()
        }
    }

    override fun hideLoading() {
        runOnUiThread {
            if (dialog == null)
                dialog = ProgressDialog(this);

            dialog!!.hide()
        }
    }

    data class CharListResult(val listChar: List<Character>, val user: User)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_characters)
    }

    var mListCharItemView = ArrayList<CharacterItemView>()

    override fun setupUI() {
        ivClose.setOnClickListener {
//            val mainIntent = Intent(this, MainMenuActivity::class.java)
//            startActivity(mainIntent)
            finish()
        }
        mListCharItemView.addAll(arrayOf(cvChar1, cvChar2, cvChar3, cvChar4, cvChar5, cvChar6, cvChar7, cvChar8).toList())

        val listResultObservable = NetworkClient.instance.getCurrentUser().firstElement().toObservable()
                .flatMap { user: User ->
                    DataCenter.instance.getUserCharacters()
                            .flatMap { listChar ->
                                Observable.just(CharListResult(listChar, user))
                            }
                }
                .doOnNext { result ->
                    mListCharItemView.forEachIndexed { index, characterItemView ->
                        characterItemView.setDelegate(this)
                        characterItemView.context = applicationContext
                        if (index < result.listChar.size) {
                            val character = result.listChar[index]
                            characterItemView.setCharacter(character, character.char_ID == result.user.active_char_ID)
                            if (character.char_ID == result.user.active_char_ID) {
                                characterItemView.doFirstSetupEvent()
                            }
                        }
                    }
                }

        compositeDisposable.add(listResultObservable.firstElement().toSingle()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe())
    }

    private fun findAllCharItemViews(viewGroup: ViewGroup): List<CharacterItemView> {
        val count = viewGroup.childCount
        val listItem = ArrayList<CharacterItemView>()
        for (i in 0 until count) {
            val view = viewGroup.getChildAt(i)
            if (view is ViewGroup)
                findAllCharItemViews(view)
            else if (view is CharacterItemView) {
                view.index = listItem.size
                listItem.add(view as CharacterItemView)
            }
        }
        return listItem
    }

}