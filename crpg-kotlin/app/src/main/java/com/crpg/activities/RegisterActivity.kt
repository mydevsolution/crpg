package com.crpg.activities

import android.content.Intent
import android.os.Bundle
import com.crpg.R
import com.crpg.data.DataCenter
import com.crpg.utils.Utils
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_register.*
import java.util.concurrent.TimeUnit

class RegisterActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    override fun setupUI() {
        compositeDisposable.add(RxView.clicks(tvRegister)
                .filter {
                    val emptyCheck = Utils.checkEmptyUserAndPassword(etUsername.text.toString(), etPassword.text.toString())
                    if (!emptyCheck.isEmpty()) {
                        showToast(emptyCheck, false)
                    }
                    emptyCheck.isEmpty()
                }
                .doOnNext {
                    tvRegister.isClickable = false
                    showToast("Please wait register...", true)
                }
                .flatMap({
                    DataCenter.instance.signUp(Utils
                            .mapUsernameToEmail(etUsername.text.toString()),
                            etPassword.text.toString())
                })
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ user ->
                    tvRegister.isClickable = true
                    if (user.error.isEmpty()) {
                        val intent = Intent(this@RegisterActivity, MainMenuActivity::class.java)
                        startActivity(intent)
                    } else {
                        showToast("Error: " + user.error, false)
                    }
                }))

        compositeDisposable.add(RxView.clicks(ivEdit)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ v ->
                    val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }))
    }
}
