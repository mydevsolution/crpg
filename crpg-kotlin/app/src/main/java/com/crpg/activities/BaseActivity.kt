package com.crpg.activities

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.widget.Toast
import io.reactivex.disposables.CompositeDisposable


@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity(), ViewTreeObserver.OnGlobalLayoutListener {

    internal var compositeDisposable = CompositeDisposable()

    @SuppressLint("ObsoleteSdkInt")
    override fun onCreate(savedInstanceState: Bundle?) {
        hideSystemUI()
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)

        val rootView = window.decorView.rootView
        rootView.viewTreeObserver.addOnGlobalLayoutListener(this)

        window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            hideSystemUI()
        }
    }

    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    fun hideSystemUI() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    abstract fun setupUI()

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    fun showToast(message: String, isShort: Boolean) {
        if (isShort)
            runOnUiThread { Toast.makeText(this, message, Toast.LENGTH_SHORT).show(); }
        else
            runOnUiThread { Toast.makeText(this, message, Toast.LENGTH_LONG).show(); }

    }

    override fun onGlobalLayout() {
        val rootView = window.decorView.rootView
        rootView.viewTreeObserver.removeOnGlobalLayoutListener(this)
        setupUI()
    }
}