package com.crpg.activities

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.crpg.R
import com.crpg.adapters.FriendsAdapter
import com.crpg.adapters.StoryRoomsAdapter
import com.crpg.data.DataCenter
import com.crpg.data.StoryRoom
import com.crpg.network.NetworkClient
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main_menu.*
import kotlinx.android.synthetic.main.layout_create_room.*
import kotlinx.android.synthetic.main.layout_friend_list.*
import android.content.DialogInterface
import android.widget.EditText
import android.view.LayoutInflater
import android.widget.RadioButton
import kotlinx.android.synthetic.main.activity_story_room.*


class MainMenuActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
    }

    override fun setupUI() {

        ivCheckMark.setOnClickListener {
            ivCheckMark.isSelected = !ivCheckMark.isSelected
        }

        compositeDisposable.add(RxView.clicks(rlCharacters).subscribe({
            val charIntent = Intent(this, CharactersActivity::class.java)
            startActivity(charIntent)
//            finish()
        }))

        compositeDisposable.add(RxView.clicks(tvCreateRoom)
                .subscribe({
                    if (createRoomPopup.visibility != View.VISIBLE) {
                        etRoomName.text.clear()
                        etRoomPassword.text.clear()
                        etInformation.text.clear()
                        createRoomPopup.visibility = View.VISIBLE
                    }
                }))


        compositeDisposable.add(Observable.merge(RxView.clicks(emptyViewPopup), RxView.clicks(llBottomPopup))
                .subscribe({
                    createRoomPopup.visibility = View.INVISIBLE
                }))

        compositeDisposable.add(RxView.clicks(tvCreate)
                .doOnNext { tvCreate.isClickable = false }
                .flatMap {
                    NetworkClient.instance.getCurrentUser().firstElement().toObservable()
                }
                .filter { user ->
                    if (user.characters.isEmpty()) {
                        showToast("You need to make your first character to join RPG chats, please go to the characters section in the bottom right corner.", false)
                        tvCreate.isClickable = true
                    }
                    user.characters.isNotEmpty()
                }
                .filter {
                    val roomName = etRoomName.text.toString()
                    if (roomName.isEmpty()) {
                        showToast("Please input room's name!", true)
                        tvCreate.isClickable = true
                        false
                    }  else if (ivCheckMark.isSelected && etRoomPassword.text.isBlank()) {
                        showToast("Please input room's password!", true)
                        tvCreate.isClickable = true
                        false
                    } else {
                        true
                    }
                }
                .flatMap {
                    val roomName = etRoomName.text.toString()
                    val roomInfor = etInformation.text.toString()
                    val roomPass = etRoomPassword.text.toString()
                    DataCenter.instance.createNewRoom(roomName, roomInfor, roomPass)
                }
                .subscribe({ room ->
                    tvCreate.isClickable = true
                    if (!room.error.isEmpty()) {
                        showToast(room.error, false)
                    } else {
                        createRoomPopup.visibility = View.INVISIBLE
                        gotoRoom(room)
                    }
                })
        )

        // Show storyrooms
        val roomsAdapter = StoryRoomsAdapter(ArrayList())
        rvStoryRooms.layoutManager = LinearLayoutManager(this)
        rvStoryRooms.adapter = roomsAdapter
        compositeDisposable.add(
                NetworkClient.instance.getCurrentUser()
                        .flatMap { DataCenter.instance.getAllRooms() }
                        .subscribe { rooms ->
                            val listFavorite = NetworkClient.instance.getTempUser().favorite_rooms.map { baseIDModel -> baseIDModel.key }
                            val sortedRooms = rooms.filter { room -> listFavorite.contains(room.room_ID) && !room.room_deleted }
                                    .sortedBy { room -> room.room_title }
                                    .toMutableList()
                            sortedRooms.addAll(rooms.filter { room -> !listFavorite.contains(room.room_ID) && !room.room_deleted  }.sortedBy { room -> room.room_title })
                            roomsAdapter.listRoom.clear()
                            roomsAdapter.listRoom.addAll(sortedRooms)
                            roomsAdapter.notifyDataSetChanged()
                        })

        compositeDisposable.add(RxView.clicks(ivLogout)
                .flatMap { showLogoutDialog() }
                .filter { isLogout -> isLogout }
                .doOnNext { NetworkClient.instance.logout() }
                .doOnNext { hideSystemUI() }
                .doOnNext {
                    val loginIntent = Intent(this, LoginActivity::class.java)
                    startActivity(loginIntent)
                    finish()
                }
                .subscribe())

        compositeDisposable.add(roomsAdapter.getViewClickedObservable()
                .filter { createRoomPopup.visibility != View.VISIBLE }
                .map { pos -> roomsAdapter.listRoom.get(pos) }
                .doOnNext { room ->
                    if (room.room_pass.isEmpty() || room.room_admin.equals(NetworkClient.instance.getTempUser().active_char_ID)) {
                        gotoRoom(room)
                    } else {
                        showRoomEnterPasswordDialog(room)
                    }
                }.subscribe())

//        compositeDisposable.add(roomsAdapter.getStarClickedObservable()
//                .map { pos -> roomsAdapter.listRoom.get(pos) }
//                .flatMap { room ->
//                    val user = NetworkClient.instance.getTempUser()
//                    user.favorite_rooms
//                }
//                .doOnNext { room ->
//                    gotoRoom(room)
//                }.subscribe())


        val friendAdapter = FriendsAdapter(ArrayList())
        rvFriendList.layoutManager = LinearLayoutManager(this)
        rvFriendList.adapter = friendAdapter
        compositeDisposable.add(DataCenter.instance.getMyFriends().subscribe { listUser ->
            tvFriendOnline.text = listUser.size.toString()
            friendAdapter.listFriends.clear()
            friendAdapter.listFriends.addAll(listUser)
            friendAdapter.notifyDataSetChanged()
        })

        compositeDisposable.add(friendAdapter.getItemClickObservable().subscribe { char ->
            val intent = Intent(this, CharacterEditActivity::class.java)
            intent.putExtra(CharacterEditActivity.CHAR_ID_EXTRA, char.char_ID)
            startActivity(intent)
        })

        compositeDisposable.add(Observable.merge(RxView.clicks(vEmptyFlLeft), RxView.clicks(vEmptyFlRight), RxView.clicks(vEmptyFlBottom))
                .doOnNext { rlFriendList.visibility = View.INVISIBLE }.subscribe())

        compositeDisposable.add(RxView.clicks(rlFriends).subscribe {
            rlFriendList.visibility = View.VISIBLE
        })

    }

    private fun showRoomEnterPasswordDialog(room: StoryRoom) {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.layout_enter_room_password, null)
        val etPassword = dialogView.findViewById<EditText>(R.id.etRoomPassword)

        dialogBuilder.setView(dialogView)

//        dialogBuilder.setTitle("Input Password")
        dialogBuilder.setPositiveButton("OK") { dialog, whichButton ->
            if (etPassword.text.toString().equals(room.room_pass)) {
                gotoRoom(room)
            } else {
                showToast("Wrong password!", false)
            }
        }
        dialogBuilder.setNegativeButton("Cancel") { dialog, whichButton ->

        }
        val b = dialogBuilder.create()
        b.show()
    }

    private fun gotoRoom(room: StoryRoom) {
        if (NetworkClient.instance.getTempUser().characters.isEmpty()) {
            showToast("You need to make your first character to join RPG chats, please go to the characters section in the bottom right corner.", false)
            return
        }
        val roomIntent = Intent(this, StoryRoomActivity::class.java)
        roomIntent.putExtra(StoryRoomActivity.ROOM_ID_EXTRA, room.room_ID)
        roomIntent.putExtra(StoryRoomActivity.ROOM_TITLE_EXTRA, room.room_title)
        startActivity(roomIntent)
//        finish()
        println("gotoRoom $room.room_ID")
    }



    private fun showLogoutDialog(): Observable<Boolean> {
        return Observable.create { observer ->
            val alertDialog = AlertDialog.Builder(this@MainMenuActivity).create()
            alertDialog.setTitle("Logout")
            alertDialog.setMessage("Are you sure to logout?")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK")
            { dialog, which ->
                dialog.dismiss()
                observer.onNext(true)
            }
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel") { dialog, which ->
                dialog.dismiss()
                observer.onNext(false)
            }

            alertDialog.show()
        }
    }

}
