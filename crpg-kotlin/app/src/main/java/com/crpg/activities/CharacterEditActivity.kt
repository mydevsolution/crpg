package com.crpg.activities

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.crpg.R
import com.crpg.data.Character
import com.crpg.data.DataCenter
import com.crpg.network.NetworkClient
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.activity_character_edit.*
import java.util.concurrent.TimeUnit

class CharacterEditActivity : BaseActivity() {

    var mListEditImageView = ArrayList<ImageView>()
    var mListEditText = ArrayList<EditText>()
    val canEditSubject = BehaviorSubject.createDefault(false)
    var mCharID = ""

    override fun setupUI() {
        mListEditImageView.addAll(arrayOf(ivEditName, ivEditBackStory, ivEditAbility, ivEditAvatar, ivEditItem))
        mListEditText.addAll(arrayOf(etName, etAbility, etBackStory, etItem))

        ivClose.setOnClickListener {
            //            val intent = Intent(this, CharactersActivity::class.java)
//            startActivity(intent)
            finish()
        }

        compositeDisposable.add(DataCenter.instance.getCharacter(mCharID)
                .firstElement().toSingle().toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { char ->
                    etName.setText(char.char_name)
                    Glide.with(this@CharacterEditActivity).load(char.avatar).placeholder(R.drawable.img_sample_user)
                            .into(ivAvatar)
                    etBackStory.setText(char.back_story)
                    etAbility.setText(char.ability)
                    etItem.setText(char.item)
                })


        compositeDisposable.add(Observable.merge(RxTextView.textChangeEvents(etName), RxTextView.textChangeEvents(etBackStory)
                , RxTextView.textChangeEvents(etAbility), RxTextView.textChangeEvents(etItem))
                .debounce(500, TimeUnit.MILLISECONDS)
                .flatMap {
                    val char = Character(mCharID, "", NetworkClient.instance.getCurrentUserID(), "")
                    char.char_name = etName.text.toString()
                    char.ability = etAbility.text.toString()
                    char.item = etItem.text.toString()
                    char.back_story = etBackStory.text.toString()
                    NetworkClient.instance.updateCharacter(char)
                }.subscribe())

        compositeDisposable.add(NetworkClient.instance.getCurrentUser()
                .firstElement().toObservable()
                .map { user ->
                    return@map user.characters.map { baseIDModel -> baseIDModel.key }.contains(mCharID)
                }.subscribe { isMind ->
            if (isMind) {
                ivEditName.visibility = View.VISIBLE
                ivEditBackStory.visibility = View.VISIBLE
                ivEditAvatar.visibility = View.VISIBLE
                ivEditAbility.visibility = View.VISIBLE
                ivEditItem.visibility = View.VISIBLE
            } else {
                ivEditName.visibility = View.INVISIBLE
                ivEditBackStory.visibility = View.INVISIBLE
                ivEditAvatar.visibility = View.INVISIBLE
                ivEditAbility.visibility = View.INVISIBLE
                ivEditItem.visibility = View.INVISIBLE
                etName.isEnabled = false
                etBackStory.isEnabled = false
                etAbility.isEnabled = false
                etItem.isEnabled = false
            }
        })

        ivEditName.setOnClickListener {
            etName.requestFocus()
        }

        ivEditBackStory.setOnClickListener {
            etBackStory.requestFocus()
        }

        ivEditItem.setOnClickListener {
            etItem.requestFocus()
        }

        ivEditAbility.setOnClickListener {
            etAbility.requestFocus()
        }

        ivEditAvatar.setOnClickListener {
            chooseAvatarView.visibility = View.VISIBLE
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_edit)
        mCharID = intent.getStringExtra(CHAR_ID_EXTRA)
    }

    companion object {
        val CHAR_ID_EXTRA: String = "CHAR_ID_EXTRA"
    }
}
