package com.crpg.activities

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import com.crpg.R
import com.crpg.adapters.MessageAdapter
import com.crpg.adapters.RoomCharactersAdapter
import com.crpg.data.BaseIDModel
import com.crpg.data.Character
import com.crpg.data.DataCenter
import com.crpg.data.Message
import com.crpg.network.NetworkClient
import com.jakewharton.rxbinding2.view.RxView
import io.github.rockerhieu.emojicon.EmojiconGridFragment
import io.github.rockerhieu.emojicon.EmojiconsFragment
import io.github.rockerhieu.emojicon.emoji.Emojicon
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_story_room.*


class StoryRoomActivity : BaseActivity(), EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    var roomID = ""
    var senderName = ""
    var mActiveCharacter: Character = Character()

    companion object {
        val ROOM_ID_EXTRA: String = "ROOM_ID_EXTRA"
        val ROOM_TITLE_EXTRA: String = "ROOM_TITLE_EXTRA"
    }

    var isPanelOpen = false

    @SuppressLint("SetTextI18n")
    override fun setupUI() {
        compositeDisposable.add(RxView.clicks(ivClose)
                .flatMap { o ->
                    DataCenter.instance.exitRoom(roomID)
                }
                .subscribe {
                    val mainIntent = Intent(this, MainMenuActivity::class.java)
                    startActivity(mainIntent)
                    finish()
                })

        compositeDisposable.add(RxView.clicks(ivPanelToggle)
                .subscribe {
                    togglePanel()
                })

        compositeDisposable.add(RxView.clicks(ivEmoji)
                .subscribe {
                    if (emojicons.visibility == View.VISIBLE) {
                        emojicons.visibility = View.INVISIBLE
                    } else
                        emojicons.visibility = View.VISIBLE
                })

        val messageAdapter = MessageAdapter(ArrayList())
        rvMessages.layoutManager = LinearLayoutManager(this)
        rvMessages.adapter = messageAdapter
        compositeDisposable.add(DataCenter.instance.getMessageFromRoom(roomID).subscribe { message ->
            messageAdapter.messages.add(message)
            messageAdapter.notifyItemInserted(messageAdapter.messages.size - 1)
            rvMessages.scrollToPosition(messageAdapter.messages.size - 1)
        })

//        var currentInput = ""
//        compositeDisposable.add(
//                RxTextView.textChanges(etInput)
//                        .throttleWithTimeout(300, TimeUnit.MILLISECONDS)
//                        .filter {  mActiveCharacter.char_name.isNotEmpty() }
//                        .filter({ etInput.text.toString().isNotEmpty() })
//                        .filter { currentInput != etInput.text.toString() }
//                        .doOnNext { _ ->
//                            replaceShortcutText(etInput.text.toString())
//                            currentInput = etInput.text.toString()
//                        }.subscribe())

        compositeDisposable.add(RxView.clicks(tvSend)
                .filter {
                    etInput.text.toString().isNotEmpty()
                }
                .filter {
                    mActiveCharacter.char_name.isNotEmpty()
                }
                .map { _ -> etInput.text.toString() }
                .flatMap { text ->
                    if (senderName.isEmpty()) {
                        NetworkClient.instance.getCurrentUser()
                                .map { user ->
                                    val message = Message()
                                    message.content = text
                                    message.sender_name = mActiveCharacter.char_name
                                    return@map message
                                }
                    } else {
                        Observable.just(senderName)
                                .map { Message(text, "", senderName, roomID, -1) }
                    }
                }
                .doOnNext { mess -> DataCenter.instance.sendMessage(mess.content, roomID, mess.sender_name) }
                .doOnNext { etInput.text.clear() }
                .subscribe())

        compositeDisposable.add(DataCenter.instance.enterRoom(roomID).subscribe())

        val friendAdapter = RoomCharactersAdapter(ArrayList(), roomID)
        rvListUser.layoutManager = LinearLayoutManager(this)
        rvListUser.adapter = friendAdapter

        compositeDisposable.add(NetworkClient.instance.getStoryRoom(roomID)
                .filter { room ->
                    if (room.room_deleted) {
                        showDialogRoomDeleteByAdmin()
                    }
                    return@filter !room.room_deleted
                }
                .subscribe { room ->
                    tvRoomTitle.text = room.room_title
                    tvNumberOfUser.text = "${room.characters.size}/15"
                    friendAdapter.setRoom(room)
                })

        compositeDisposable.add(DataCenter.instance.getRoomCharacter(roomID)
                .doOnNext { listChar ->
                    val activeChar = listChar.filter { char -> char.char_ID == NetworkClient.instance.getTempUser().active_char_ID }.firstOrNull()
                    if (activeChar != null) {
                        mActiveCharacter = activeChar
                        messageAdapter.setActiveCharacter(mActiveCharacter)
                    }
                }
                .subscribe { listUser ->
                    friendAdapter.listFriends.clear()
                    friendAdapter.listFriends.addAll(listUser)
                    friendAdapter.notifyDataSetChanged()
                })

        compositeDisposable.add(friendAdapter.getItemLongClickObservable()
                .map { pos -> friendAdapter.listFriends.get(pos) }
                .flatMap { char ->
                    NetworkClient.instance.getCurrentUser()
                            .firstElement().toObservable()
                            .flatMap { user ->
                                val friends = user.friends.map { base -> base.key }
                                if (user.characters.map { baseIDModel -> baseIDModel.key }.contains(char.char_ID)) {
                                    return@flatMap Observable.just(false)
                                } else if (friends.contains(char.char_ID)) {
                                    showToast(char.char_name + " already your friend!", true)
                                    return@flatMap Observable.just(false)
                                } else {
                                    val frs = user.friends.toMutableList()
                                    frs.add(BaseIDModel(char.char_ID, true))
                                    user.friends = frs.toList()
                                    NetworkClient.instance.updateUser(user, true)
                                            .doOnNext {
                                                showToast(char.char_name + " added as friend", true)
                                            }
                                }
                            }
                }
                .subscribe())

        compositeDisposable.add(RxView.clicks(tvRoomTitle)
                .flatMap {
                    NetworkClient.instance.getStoryRoom(roomID).firstElement().toObservable()
                            .filter { room ->
                                room.room_admin.equals(mActiveCharacter.char_ID)
                            }
                            .flatMap { room ->
                                showEditRoomDialog(room.room_title)
                                        .flatMap { chooseStr ->
                                            if (chooseStr.equals("delete")) {
                                                NetworkClient.instance.deleteRoom(roomID)
                                                finish()
                                            } else if (chooseStr.isNotEmpty()) {
                                                room.room_title = chooseStr
                                                tvRoomTitle.text = chooseStr
                                                return@flatMap NetworkClient.instance.updateStoryRoom(room)
                                            }
                                            return@flatMap Observable.just(true)
                                        }
                            }
                }.subscribe())

        compositeDisposable.add(friendAdapter.getItemClickObservable().subscribe { pos ->
            goToCharacterDetail(friendAdapter.listFriends.get(pos))
        })

    }


    fun showEditRoomDialog(title: String): Observable<String> {
        return Observable.create<String> { observer ->
            val dialogBuilder = AlertDialog.Builder(this)
            val inflater = this.layoutInflater
            val dialogView = inflater.inflate(R.layout.layout_edit_room, null)
            val etRoomTitle = dialogView.findViewById<EditText>(R.id.etRoomTitle)
            val radioDelete = dialogView.findViewById<RadioButton>(R.id.radioDelete)
            val radioEditTitle = dialogView.findViewById<RadioButton>(R.id.radioEditTitle)
            dialogBuilder.setView(dialogView)
            etRoomTitle.setText(title)
            radioDelete.isChecked = false
            radioEditTitle.isChecked = true

            dialogBuilder.setTitle("Edit Room")
//        dialogBuilder.setMessage("Enter text below")
            dialogBuilder.setPositiveButton("OK") { dialog, whichButton ->
                if (radioDelete.isChecked) {
                    observer.onNext("delete")
                } else if (radioEditTitle.isChecked) {
                    observer.onNext(etRoomTitle.text.toString())
                }
                observer.onComplete()
            }
            dialogBuilder.setNegativeButton("Cancel") { dialog, whichButton ->
                observer.onComplete()
            }
            val b = dialogBuilder.create()
            b.show()
        }
    }

    fun goToCharacterDetail(character: Character) {
        val intent = Intent(this, CharacterEditActivity::class.java)
        intent.putExtra(CharacterEditActivity.CHAR_ID_EXTRA, character.char_ID)
        startActivity(intent)
    }

    fun togglePanel() {
        isPanelOpen = !isPanelOpen
        if (isPanelOpen) {
            val r = resources
            val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50f, r.displayMetrics)
            val delX = rlPanel.width - px
            rlPanel.animate().translationX(delX).start()
            ivPanelToggle.setImageResource(R.drawable.ic_open)
        } else {
            rlPanel.animate().translationX(0f).start()
            ivPanelToggle.setImageResource(R.drawable.ic_hide)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_room)

        roomID = intent.getStringExtra(ROOM_ID_EXTRA)
        val roomTitle = intent.getStringExtra(ROOM_TITLE_EXTRA)
        tvRoomTitle.text = roomTitle
        setEmojiconFragment(true)
    }

    override fun onEmojiconClicked(emojicon: Emojicon?) {
        if (emojicon != null) {
            EmojiconsFragment.input(etInput, emojicon)
        }
    }

    override fun onEmojiconBackspaceClicked(v: View?) {

    }

    private fun setEmojiconFragment(useSystemDefault: Boolean) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit()
    }

    fun showDialogRoomDeleteByAdmin() {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage("This room just deleted by admin!")
        dialogBuilder.setTitle("Room deleted")
//        dialogBuilder.setMessage("Enter text below")
        dialogBuilder.setPositiveButton("OK") { dialog, whichButton ->
            finish()
        }
        val b = dialogBuilder.create()
        b.show()
    }

}
