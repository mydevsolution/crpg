package com.crpg.network

import android.text.TextUtils
import com.crpg.data.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

import io.reactivex.Observable


class NetworkClient private constructor() {
    private val mAuth: FirebaseAuth
    private val mDatabase: FirebaseDatabase
    private var mCurrentUser: FirebaseUser? = null
    private val mConvertUtils = ConverterUtils.instance
    private var mUser: User = User()
    private var mActiveCharacter: Character= Character()

    private val USER_REF = "users"
    private val CHARACTERS_REF = "characters"
    private val STORY_ROOMS_REF = "story_rooms"
    private val MESSAGES_REF = "messages"

    init {
        mAuth = FirebaseAuth.getInstance()
        mCurrentUser = mAuth.currentUser
        mDatabase = FirebaseDatabase.getInstance()
    }

    fun getCurrentUserID(): String {
        if (mCurrentUser != null)
            return mCurrentUser!!.uid
        return ""
    }

    fun getTempUser() : User {
        return mUser
    }

    fun getActiveCharacter(): Character {
        return mActiveCharacter
    }


    fun createNewUser(username: String, password: String): Observable<String> {
        return Observable.create { observer ->
            mAuth.createUserWithEmailAndPassword(username, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val user = mAuth.currentUser
                            mCurrentUser = user
                            observer.onNext("")
                            observer.onComplete()
                        } else {
                            observer.onNext(task.exception!!.localizedMessage)
                        }
                    }
        }
    }

    fun logout() {
        mAuth.signOut()
        mCurrentUser = null
    }

    fun signIn(username: String, password: String): Observable<String> {
        return Observable.create { observer ->
            mAuth.signInWithEmailAndPassword(username, password)
                    .addOnCompleteListener { task ->
                        val user = mAuth.currentUser
                        if (task.isSuccessful && user != null) {
                            mCurrentUser = user
                            observer.onNext("")
                        } else {
                            observer.onNext(task.exception!!.localizedMessage)
                        }
                        observer.onComplete()
                    }
        }
    }

    /**
     * User
     */

    fun updateUser(user: User, forceSet: Boolean): Observable<User> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(USER_REF)
            if (user.user_ID.isEmpty()) {
                user.user_ID = ref.push().key
            }

            if (forceSet) {
                ref.child(user.user_ID).setValue(user.toJSON())
                observer.onNext(user)
                observer.onComplete()
            } else {
                ref.child(user.user_ID).updateChildren(user.toJSON(), { databaseError, _ ->
                    if (databaseError == null) {
                        observer.onNext(user)
                    } else {
                        user.error = databaseError.message
                        observer.onNext(user)
                        println(databaseError.message)
                    }
                    observer.onComplete()
                })
            }
            mUser = user
        }
    }

    fun getUser(userID: String): Observable<User> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(USER_REF)
            val listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
//                    val user = dataSnapshot.getValue(User::class.java)
                    val user = ConverterUtils.instance.userFromDataSnapshot(dataSnapshot)
//                    if (user != null) {
                    user.user_ID = userID
                    observer.onNext(user)
                    if (userID.equals(getCurrentUserID())) {
                        mUser = user
                    }
//                    } else {
//                        observer.onNext(User())
//                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    println("loadPost:onCancelled ${databaseError.toException()}")
                }
            }
            ref.child(userID).addValueEventListener(listener)
        }
    }

    fun getAllUser(): Observable<List<User>> {
        return Observable.create<List<User>> { observer ->
            val ref = mDatabase.reference.child(USER_REF)
            val listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val listUser = ConverterUtils.instance.listUserFromSnapShot(dataSnapshot)
                    observer.onNext(listUser)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    println("loadPost:onCancelled ${databaseError.toException()}")
                }
            }
            ref.addValueEventListener(listener)
        }
    }

    fun getCurrentUser(): Observable<User> {
        return if (mCurrentUser != null) {
            getUser(mCurrentUser!!.uid)
        } else {
            Observable.just(User())
        }
    }

    /**
     * Characters
     */

    fun createCharacter(): Observable<Character> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(CHARACTERS_REF)
            val userRef = mDatabase.reference.child(USER_REF)
            val charID = ref.push().key
            val char = Character(charID, "", getCurrentUserID(), "")
            ref.child(charID).updateChildren(char.toJSON(), { databaseError, _ ->
                if (databaseError == null) {
                    observer.onNext(char)
                    val mapper: MutableMap<String, Any> = mutableMapOf()
                    mapper.put(charID, true)
                    userRef.child(getCurrentUserID()).child("characters").updateChildren(mapper)
                } else {
                    char.error = databaseError.message
                    observer.onNext(char)
                    println(databaseError.message)
                }
                observer.onComplete()
            })
        }
    }

    fun getCharacters(user: User): Observable<ArrayList<Character>> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(CHARACTERS_REF)
            val listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    var characters = ArrayList<Character>()
                    dataSnapshot.children.forEach { snap ->
                        val char = dataSnapshot.getValue(Character::class.java)
                        if (char != null) {
                            characters.add(char)
                        }
                    }
                    observer.onNext(characters)
                    observer.onComplete()
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    println("loadPost:onCancelled ${databaseError.toException()}")
                }
            }
            ref.addListenerForSingleValueEvent(listener)
        }
    }

    fun getCharacter(char_ID: String): Observable<Character> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(CHARACTERS_REF)
            val listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val char = mConvertUtils.characterFromSnapShot(dataSnapshot)

//                    if (char != null) {
                        observer.onNext(char)
//                    } else {
//                        observer.onNext(Character())
//                        println("getCharacter: null")
//                    }
//                    observer.onComplete()
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    println("loadPost:onCancelled ${databaseError.toException()}")
                    observer.onComplete()
                }
            }
            ref.child(char_ID).addValueEventListener(listener)
        }
    }

    fun updateCharacter(character: Character): Observable<String> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(CHARACTERS_REF)
            if (character.char_ID.isEmpty()) {
                character.char_ID = ref.push().key
            }
            ref.child(character.char_ID).updateChildren(character.toJSON(), { databaseError, _ ->
                if (databaseError != null) {
                    observer.onNext(databaseError.message)
                } else {
                    observer.onNext("")
                }
                observer.onComplete()
            })
        }
    }

    /**
     * StoryRoom
     */

    fun getStoryRoom(room_ID: String): Observable<StoryRoom> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(STORY_ROOMS_REF)
            val listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val storyRoom = mConvertUtils.roomFromSnapShot(dataSnapshot)
                    storyRoom.room_ID = room_ID
//                    if (storyRoom != null) {
                    observer.onNext(storyRoom)
//                    } else {
//                        observer.onNext(StoryRoom())
//                        println("getStoryRoom: null")
//                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    println("loadPost:onCancelled ${databaseError.toException()}")
                }
            }
            ref.child(room_ID)
                    .addValueEventListener(listener)
        }
    }

    fun getStoryRooms(user: User): Observable<List<StoryRoom>> {
        val listRoomObservable = ArrayList<Observable<StoryRoom>>()
        user.story_rooms.forEach { model ->
            listRoomObservable.add(getStoryRoom(model.key))
        }
        return Observable.zip(listRoomObservable, { result ->
            result.filter { value -> value is StoryRoom }.map { value -> value as StoryRoom }
        })
    }

    fun getAllRooms(): Observable<List<StoryRoom>> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(STORY_ROOMS_REF)
            val listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val listRoom = mConvertUtils.listRoomFromSnapShot(dataSnapshot)
//                    if (storyRoom != null) {
                    observer.onNext(listRoom)
//                    } else {
//                        observer.onNext(StoryRoom())
//                        println("getStoryRoom: null")
//                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    println("loadPost:onCancelled ${databaseError.toException()}")
                }
            }
            ref.addValueEventListener(listener)
        }
    }

    fun updateStoryRoom(room: StoryRoom): Observable<StoryRoom> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(STORY_ROOMS_REF)
            if (room.room_ID.isEmpty()) {
                room.room_ID = ref.push().key
            }

            ref.child(room.room_ID).updateChildren(room.toJSON(), { databaseError, _ ->
                if (databaseError == null) {
                    observer.onNext(room)
                } else {
                    room.error = databaseError.message
                    observer.onNext(room)
                    println(databaseError.message)
                }
                observer.onComplete()
            })
        }
    }

    companion object {
        val instance = NetworkClient()
        private val TAG = NetworkClient::class.java.simpleName
    }

    fun getMessageFromRoom(room_ID: String): Observable<Message> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(MESSAGES_REF)
            val listener = object : ChildEventListener {
                override fun onCancelled(p0: DatabaseError?) {
                }

                override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
                }

                override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                }

                override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                    if (p0 != null) {
                        val message = mConvertUtils.messageFromSnapShot(p0)
                        observer.onNext(message)
                    }
                }

                override fun onChildRemoved(p0: DataSnapshot?) {
                }
            }

            ref.child(room_ID).addChildEventListener(listener)
        }
    }

    fun sendMessage(message: Message) {
        val ref = mDatabase.reference.child(MESSAGES_REF).child(message.receiver_ID)
        message.message_ID = ref.push().key
        ref.child(message.message_ID).setValue(message.toJSON())
    }

    fun createEmptyMessageRoom(room_ID: String) {
        val mapper: MutableMap<String, Any> = mutableMapOf()
        mapper.put(room_ID, true)
        mDatabase.reference.child(MESSAGES_REF).updateChildren(mapper)
    }

    fun deleteCharacter(char_ID: String): Observable<String> {
        return Observable.create { observer ->
            val userRef = mDatabase.reference.child(USER_REF)
            val charRef = mDatabase.reference.child(CHARACTERS_REF)
            charRef.child(char_ID).removeValue()
            userRef.child(getCurrentUserID()).child("characters").child(char_ID).removeValue { databaseError, _ ->
                if (databaseError == null) {
                    observer.onNext("")
                } else {
                    observer.onNext(databaseError.message)
                    println(databaseError.message)
                }
                observer.onComplete()
            }
        }
    }

    fun setActiveCharacter(character: Character): Observable<Character> {
        mDatabase.reference.child(USER_REF).child(getCurrentUserID()).child("active_char_ID").setValue(character.char_ID)
        return Observable.just(character)
    }

    fun enterRoom(roomID: String): Observable<String> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(STORY_ROOMS_REF)
            val mapper: MutableMap<String, Any> = mutableMapOf()
            mapper.put(mUser.active_char_ID, true)
            ref.child(roomID).child("characters").updateChildren(mapper, { databaseError, _ ->
                if (databaseError == null) {
                    observer.onNext("")
                } else {
                    observer.onNext(databaseError.message)
                    println(databaseError.message)
                }
                observer.onComplete()
            })
        }
    }

    fun exitRoom(roomID: String): Observable<String> {
        return Observable.create { observer ->
            val ref = mDatabase.reference.child(STORY_ROOMS_REF)
            val mapper: MutableMap<String, Any> = mutableMapOf()
            mapper.put(getCurrentUserID(), true)
            ref.child(roomID).child("characters").child(mUser.active_char_ID).removeValue()
            observer.onNext("")
            observer.onComplete()
//            ref.child(roomID).child("characters").child(mUser.active_char_ID).removeValue { databaseError, _ ->
//                if (databaseError == null) {
//                    observer.onNext("")
//                } else {
//                    observer.onNext(databaseError.message)
//                    println(databaseError.message)
//                }
//                observer.onComplete()
//            }
        }
    }

    fun deleteRoom(roomID: String) {
        val ref = mDatabase.reference.child(STORY_ROOMS_REF).child(roomID)
        val mapper: MutableMap<String, Any> = mutableMapOf()
        mapper.put("room_deleted", true)
        ref.updateChildren(mapper)
    }

}



