package com.crpg.utils;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import java.util.ArrayList;
import java.util.List;


public class Utils {

    public static boolean validateUserAndPassword (String username, String password) {
        return !TextUtils.isEmpty(username) && !TextUtils.isEmpty(password) && password.length() >= 6;
    }

    public static String checkEmptyUserAndPassword (String username, String password) {
        if (username.isEmpty() || password.isEmpty()) {
            return "Username and Password cannot be empty!";
        }
        return "";
    }

    public static String mapUsernameToEmail(String username) {
        return username + "@crpg.com";
    }

    public static String mapEmailToUsername(String email) {
        if (email.contains("@crpg.com")) {
            return email.replaceAll("@crpg.com", "");
        } else {
            return email;
        }
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static List<Integer> findIndexs(String text, String textToFind) {
        List<Integer> result = new ArrayList<>();
        for (int i = -1; (i = text.indexOf(textToFind, i + 1)) != -1; i++) {
            result.add(i);
        }
        return result;
    }
}
