package com.crpg.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crpg.R
import com.crpg.data.BaseIDModel
import com.crpg.data.StoryRoom
import com.crpg.network.NetworkClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.item_story_room_layout.view.*
import io.reactivex.subjects.PublishSubject
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable


/**
 * Created by Rainy on 10/17/17.
 */
class StoryRoomsAdapter(val listRoom: ArrayList<StoryRoom>) : RecyclerView.Adapter<StoryRoomsAdapter.ViewHolder>() {

    val mViewClickSubject = PublishSubject.create<Int>()
    val compositeDisposable = CompositeDisposable()

    fun getViewClickedObservable(): Observable<Int> {
        return mViewClickSubject.observeOn(AndroidSchedulers.mainThread())
    }


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryRoomsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_story_room_layout, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: StoryRoomsAdapter.ViewHolder, position: Int) {
        holder.bindItems(listRoom[position], compositeDisposable)

        Observable.merge(RxView.clicks(holder.itemView), RxView.clicks(holder.itemView.tvRoomName))
                .map { _ -> position }
                .subscribe(mViewClickSubject)
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder?) {
        super.onViewDetachedFromWindow(holder)
//        mViewClickSubject.onComplete()
        compositeDisposable.clear()
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return listRoom.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(room: StoryRoom, compositeDisposable: CompositeDisposable) {
            itemView.tvRoomName.text = room.room_title
            itemView.tvNumberOfUser.text = "${room.characters.size}/15"
            itemView.ivLock.visibility = if (room.room_pass.isNotEmpty())  View.VISIBLE else View.INVISIBLE

            compositeDisposable.add(NetworkClient.instance.getCurrentUser()
                    .subscribe { user ->
                        val isFavorite = user.favorite_rooms.filter { baseID -> room.room_ID.equals(baseID.key) }.isNotEmpty()
                        if (isFavorite)
                            itemView.ivStar.setImageResource(R.drawable.ic_star_yellow)
                        else
                            itemView.ivStar.setImageResource(R.drawable.ic_star)
                    })

            compositeDisposable.add(RxView.clicks(itemView.ivStar)
                    .flatMap {
                        val currentListFavorite = NetworkClient.instance.getTempUser().favorite_rooms.map { baseIDModel -> baseIDModel.key }.toMutableList()
                        if (currentListFavorite.contains(room.room_ID)) {
                            currentListFavorite.remove(room.room_ID)
                        } else {
                            currentListFavorite.add(room.room_ID)
                        }
                        val user = NetworkClient.instance.getTempUser()
                        user.favorite_rooms.clear()
                        user.favorite_rooms.addAll(currentListFavorite.toTypedArray().map { str -> BaseIDModel(str, true) })
                        NetworkClient.instance.updateUser(user, true)
                    }.subscribe())
        }

    }
}