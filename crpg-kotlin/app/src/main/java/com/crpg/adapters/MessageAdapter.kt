package com.crpg.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.crpg.R
import com.crpg.data.Character
import com.crpg.data.Message
import com.crpg.utils.Utils
import kotlinx.android.synthetic.main.activity_story_room.*
import kotlinx.android.synthetic.main.item_message.view.*

/**
 * Created by Rainy on 10/18/17.
 */
class MessageAdapter(val messages: ArrayList<Message>) : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

    private var mActiveCharacter: Character = Character()

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: MessageAdapter.ViewHolder, position: Int) {
        holder.bindItems(messages[position], mActiveCharacter)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return messages.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var mActiveCharacter: Character

        @SuppressLint("SetTextI18n")
        fun bindItems(message: Message, character: Character) {
            mActiveCharacter = character
            itemView.tvMessage.text = replaceShortcutText(message.sender_name + ": " + message.content)
        }

        fun replaceShortcutText(text: String): SpannableString {
            if (mActiveCharacter.char_name.isNotEmpty()) {
                var tempText = text
                if (text.contains("/me")) {
                    tempText = text.replace(mActiveCharacter.char_name + ": ", "")
                }
                val replaceText = tempText.replace("/me", mActiveCharacter.char_name)
                val str1 = SpannableString(replaceText)
                for (index in Utils.findIndexs(replaceText, mActiveCharacter.char_name)) {
                    if (!replaceText.contains(":") || index > 0) {
                        str1.setSpan(ForegroundColorSpan(Color.RED), index, index + mActiveCharacter.char_name.length, 0)
                    }
                }
                return str1
            } else {
                return SpannableString(text)
            }
        }
    }


    fun setActiveCharacter(activeChar: Character) {
        mActiveCharacter = activeChar
        notifyDataSetChanged()
    }

}