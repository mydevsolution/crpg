package com.crpg.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crpg.R
import com.crpg.data.Character
import com.crpg.data.User
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_friend_which_chat.view.*

/**
 * Created by Rainy on 10/17/17.
 */
class FriendsAdapter (val listFriends: ArrayList<Character>) : RecyclerView.Adapter<FriendsAdapter.ViewHolder>() {

    private val mViewClickSubject = PublishSubject.create<Character>()!!

    fun getItemClickObservable(): Observable<Character> {
        return mViewClickSubject
    }


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_friend_which_chat, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: FriendsAdapter.ViewHolder, position: Int) {
        holder.bindItems(listFriends[position])
        RxView.clicks(holder.itemView)
                .map { listFriends.get(position) }
                .subscribe(mViewClickSubject)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return listFriends.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(character: Character) {
            itemView.tvName.text = character.char_name
        }
    }
}