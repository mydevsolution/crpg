package com.crpg.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crpg.R
import com.crpg.data.Character
import com.crpg.data.DataCenter
import com.crpg.data.StoryRoom
import com.crpg.data.User
import com.crpg.network.NetworkClient
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_chat_user.view.*

/**
 * Created by Rainy on 10/21/17.
 */
class RoomCharactersAdapter(val listFriends: ArrayList<Character>, val roomID: String) : RecyclerView.Adapter<RoomCharactersAdapter.ViewHolder>() {

    private val mViewClickSubject = PublishSubject.create<Int>()!!
    private val mViewLongClickSubject = PublishSubject.create<Int>()!!
    private var mRoom: StoryRoom = StoryRoom()
    private val mListUser = ArrayList<User>()
    private val compositeDisposable = CompositeDisposable()
    fun getItemClickObservable(): Observable<Int> {
        return mViewClickSubject
    }

    fun getItemLongClickObservable(): Observable<Int> {
        return mViewLongClickSubject
    }

    fun setRoom(room: StoryRoom) {
        mRoom = room
        compositeDisposable.add(DataCenter.instance.getAllUser()
                .subscribe { users ->
                    mListUser.addAll(users)
                    notifyDataSetChanged()
                })
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_user, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val char = listFriends[position]
        val listFiltered = mListUser.filter { user -> user.user_ID.contains(char.user_ID) }
        if (listFiltered.isNotEmpty()){
            val user = listFiltered.first()
            holder.bindItems(char, mRoom, user)
        }
        holder.itemView.setOnLongClickListener {
            mViewLongClickSubject.onNext(position)
            return@setOnLongClickListener true
        }

        holder.itemView.setOnClickListener {
            mViewClickSubject.onNext(position)
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return listFriends.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(character: Character, room: StoryRoom, user: User) {
            if (character.char_ID.equals(room.room_admin)) {
                itemView.tvUsername.text = user.username + " (admin)"
            } else {
                itemView.tvUsername.text = user.username
            }
            itemView.tvCharacterName.text = character.char_name
        }
    }
}