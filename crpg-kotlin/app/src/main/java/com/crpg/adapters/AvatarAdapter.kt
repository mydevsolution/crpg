package com.crpg.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.*


/**
 * Created by Rainy on 11/4/17.
 */
class AvatarAdapter(val listResource: List<Int>, val context: Context) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val imageView = ImageView(context)
        imageView.setImageResource(listResource[position])
        imageView.layoutParams = LinearLayout.LayoutParams(400, 400)
        return imageView
    }

    override fun getItem(position: Int): Any {
        return listResource[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listResource.size
    }
}